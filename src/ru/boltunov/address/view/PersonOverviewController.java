package ru.boltunov.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import ru.boltunov.address.MainApp;
import ru.boltunov.address.model.Person;

public class PersonOverviewController {
	
	@FXML
	private TableView<Person> personTable;
	@FXML
	private TableColumn<Person, String> nameColumn;
	@FXML
	private TableColumn<Person, String> surnameColumn;
	@FXML
	private Label surnameLabel;
	@FXML
	private Label nameLabel;
	@FXML
	private Label patronymicLabel;
	@FXML
	private Label cityLabel;
	@FXML
	private Label streetLabel;
	@FXML
	private Label houseLabel;
	@FXML
	private Label postalLabel;
	@FXML
	private Label phoneLabel;
	@FXML
	private Label birthdayLabel;
	@FXML
	private Button deleteButton;
	@FXML
	private Button addButton;
	@FXML
	private Button editButton;
	
	private MainApp mainApp;
		
	public PersonOverviewController() {
		
    }
	
	@FXML
	public void initialize(){
		// Initialize the person table with the two columns.
        nameColumn.setCellValueFactory(cellData -> cellData.getValue().getName());
        surnameColumn.setCellValueFactory(cellData -> cellData.getValue().getSurname());
        
        showPersonDetails(null);
        
        personTable.getSelectionModel()
        	.selectedItemProperty()
        	.addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
	}
	
	 public void setMainApp(MainApp mainApp) {
	        this.mainApp = mainApp;
	        // Add observable list data to the table
	        personTable.setItems(mainApp.getPersonData());
	 }
	 
	 public void showPersonDetails(Person person){
		 if (person != null){
			 surnameLabel.setText(person.getSurname().get());
			 nameLabel.setText(person.getName().get());
		 }
		 else
		 {
			 surnameLabel.setText("empty");
		 }
	 }
	 
	 @FXML
	 public void handleDeletePerson(){
		 int selectedIndex = personTable.getSelectionModel().getSelectedIndex();
		 if (selectedIndex > -1)
			 personTable.getItems().remove(selectedIndex);
	 }
	 
	 public void addPerson(){
		Person person = new Person("", "", ""); 
		 
		boolean okClicked = mainApp.showPersonEditDialog(person, "Добавление записи");		
		if (okClicked){
			mainApp.getPersonData().add(person);
		}
	 }
	 
	 public void editPerson(){
		 int selItem = personTable.getSelectionModel().getSelectedIndex();
		 
		 if (selItem > -1){
			 Person person = personTable.getItems().get(selItem);
			 boolean okClicked = mainApp.showPersonEditDialog(person, "Добавление записи");
			 
			 if (okClicked){
				 showPersonDetails(person);
			 }
			 
		 }
		 
		 
	 }
	 
	 
}
