package ru.boltunov.address.view;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import ru.boltunov.address.model.*;


public class PersonEditDialogController {
	private Person person;
	private Stage dialogStage;
	private boolean OkClicked = false;
	
	@FXML
	private TextField surnameField;
	@FXML
	private TextField nameField;
	@FXML
	private TextField patronymicField;
	@FXML
	private TextField cityField;
	@FXML
	private TextField streetField;
	@FXML
	private TextField houseField;
	@FXML
	private TextField postalField;
	@FXML
	private TextField phoneField;
	@FXML
	private TextField birthdayField;
	
	@FXML
	public void initialize(){
	}
	
	public void setPerson(Person person){
		if (person != null)
		{
			this.person = person;
			this.surnameField.setText(person.getSurname().get());
			this.nameField.setText(person.getName().get());
		}
	}
	
	public void setDialogStage(Stage dialogStage){
		this.dialogStage = dialogStage;
	}
	
	public boolean isOkClicked(){
		return OkClicked;
	}
	
	public void okClicked(){
		this.person.setName(nameField.getText());
		this.person.setSurname(surnameField.getText());
		
		
		OkClicked = true;
		dialogStage.close();
	}
	
	public void cancelClicked(){
		dialogStage.close();
	}
}
