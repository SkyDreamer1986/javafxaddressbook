package ru.boltunov.address.model;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {
	private final StringProperty surname;
    private final StringProperty name;
    private final StringProperty patronymic;
    private final StringProperty city;
    private final StringProperty street;
    private final IntegerProperty houseNumber;
    private final IntegerProperty postalCode;
    private final StringProperty phone;
    private final ObjectProperty<LocalDate> birthday;
	
    public StringProperty getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname.set(surname);
	}
	public StringProperty getName() {
		return name;
	}
	public void setName(String name) {
		this.name.set(name);
	}
	public String getPatronymic() {
		return patronymic.get();
	}
	public void setPatronymic(String patronymic) {
		this.patronymic.set(patronymic);
	}
	public String getCity() {
		return city.get();
	}
	public void setCity(String city) {
		this.city.set(city);
	}
	public String getStreet() {
		return street.get();
	}
	public void setStreet(String street) {
		this.street.set(street);
	}
	public Integer getHouseNumber() {
		return houseNumber.get();
	}
	public void setHouseNumber(Integer houseNumber) {
		this.houseNumber.set(houseNumber);
	}
	public Integer getPostalCode() {
		return postalCode.get();
	}
	public void setPostalCode(Integer postalCode) {
		this.postalCode.set(postalCode);
	}
	public StringProperty getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone.set(phone);
	}
	public ObjectProperty<LocalDate> getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday.set(birthday);
	} 
	
    public Person(String surname, String name, String patronymic){
    	this.surname = new SimpleStringProperty(surname);
    	this.name = new SimpleStringProperty(name);
    	this.patronymic = new SimpleStringProperty(patronymic);
    	
    	this.city = new SimpleStringProperty("");
    	this.street = new SimpleStringProperty("");
    	this.houseNumber = new SimpleIntegerProperty(0);
    	this.postalCode = new SimpleIntegerProperty(0);
    	this.phone = new SimpleStringProperty("");
    	this.birthday = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));
    }
}
