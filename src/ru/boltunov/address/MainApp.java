package ru.boltunov.address;

import java.io.IOException;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.boltunov.address.model.*;
import ru.boltunov.address.view.*;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	
	private ObservableList<Person> personData = FXCollections.observableArrayList();
	
	public MainApp() {
        // Add some sample data
        personData.add(new Person("Hans", "Muster", null));
        personData.add(new Person("Ruth", "Mueller", null));
        personData.add(new Person("Heinz", "Kurz", null));
        personData.add(new Person("Cornelia", "Meier", null));
        personData.add(new Person("Werner", "Meyer", null));
        personData.add(new Person("Lydia", "Kunz", null));
        personData.add(new Person("Anna", "Best", null));
        personData.add(new Person("Stefan", "Meier", null));
        personData.add(new Person("Martin", "Mueller", null));
    }
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("�������� �����");
		
		initRootLayout();
		showPersonOverview();
	}
	
	public void initRootLayout(){
		try {
			FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            
            rootLayout = (BorderPane) loader.load();
            Scene scene = new Scene(rootLayout);
            
            primaryStage.setScene(scene);
            primaryStage.show();
		}
		
		catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public void showPersonOverview() {
        try {
            // Load person overview.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/PersonOverview.fxml"));
            AnchorPane personOverview = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(personOverview);
            
            PersonOverviewController controller = loader.getController();
            controller.setMainApp(this);
            
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public boolean showPersonEditDialog(Person person, String title){
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/PersonEditDialog.fxml"));
			
			AnchorPane page = (AnchorPane)loader.load();
			
			Stage dialogStage = new Stage();
			dialogStage.setTitle(title);
			
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			
			PersonEditDialogController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setPerson(person);
			
			dialogStage.showAndWait();
			return controller.isOkClicked();
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Stage getPrimaryStage() {
        return primaryStage;
    }
	
	public ObservableList<Person> getPersonData() {
        return personData;
    }
	
	public static void main(String[] args) {
		launch(args);
	}
}
